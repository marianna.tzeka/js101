# Girl Develop It JS101

*Note: incomplete. Slides, materials, and homework for classes 3 and 4 aren't done yet.*

These slides are slightly modified from Pamela Fox's JS101 materials (found [here](https://github.com/pamelafox/teaching-materials)) to fit the GDI teaching template.

JS101 is meant to be taught in 4 classes, approximately 2-2.5 hours each (a total of 8-10 hours of instruction). Each session includes workshop slides with lecture notes, a series of in-class exercises, and homework to complete between classes.

*Class description and instructions for use pending slide completion.*

## Class website

The class website and slides are available hosted [here](http://gdichicago.com/classes/js101/index.html). You can teach this class using everything on that page.

The website includes slides, exercises, homework, and additional reading for each class.

**View notes on slides**: hit 's' and a new window will pop up with a presenter view, including notes, a timer, and some other useful goodies.

## Dependencies

All CSS and JavaScript used for this are hosted on AWS. You don't have to worry about them.

**Reveal.js**

These slides use [Reveal.js](https://github.com/hakimel/reveal.js) that has been edited for the GDI brand. You don't have to do anything to make it work, it should be there already.

If you don't have an internet connection, or you're worried about the internet not work, load the slides before you arrive at the workshop.